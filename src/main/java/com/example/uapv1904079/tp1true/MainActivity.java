package com.example.uapv1904079.tp1true;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView countries;
    CountryList countryList = new CountryList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        countries = (ListView) findViewById(R.id.countriesList);

        final ArrayAdapter<String> countriesAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                countryList.getNameArray());

        countries.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent showCountry;
                showCountry = new Intent(view.getContext(), CountryActivity.class);
                showCountry.putExtra("pays", countriesAdapter.getItem(position).toString());
                startActivity(showCountry);
            }
        });


        countries.setAdapter(countriesAdapter);

    }

}
