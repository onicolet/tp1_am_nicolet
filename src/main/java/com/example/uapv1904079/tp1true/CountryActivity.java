package com.example.uapv1904079.tp1true;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);


        Intent intent = getIntent();
        final String country = intent.getStringExtra("pays");

        final TextView countryTxt = (TextView) findViewById(R.id.countryTxt);
        countryTxt.setText(country);

        Country thisCountry = CountryList.getCountry(country);

        EditText cityInp = (EditText) findViewById(R.id.capitaleInput);
        cityInp.setText(thisCountry.getmCapital());

        EditText areaInp = (EditText) findViewById(R.id.superficieInput);
        areaInp.setText(Integer.toString(thisCountry.getmArea()));

        EditText moneyInp = (EditText) findViewById(R.id.moneyInput);
        moneyInp.setText(thisCountry.getmCurrency());

        EditText languageInp = (EditText) findViewById(R.id.langueInput);
        languageInp.setText(thisCountry.getmLanguage());

        EditText popInp = (EditText) findViewById(R.id.popInput);
        popInp.setText(Integer.toString(thisCountry.getmPopulation()));

        int flagIdentifier = getResources().getIdentifier(thisCountry.getmImgFile(), "drawable", getPackageName());
        ImageView flagPic = (ImageView) findViewById(R.id.flag);
        flagPic.setImageResource(flagIdentifier);

        Button saveButton = findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                Country thisCountry = CountryList.getCountry(country);

                EditText cityInp = (EditText) findViewById(R.id.capitaleInput);
                thisCountry.setmCapital(cityInp.getText().toString());

                EditText areaInp = (EditText) findViewById(R.id.superficieInput);
                thisCountry.setmArea(Integer.parseInt(areaInp.getText().toString()));

                EditText moneyInp = (EditText) findViewById(R.id.moneyInput);
                thisCountry.setmCurrency(moneyInp.getText().toString());

                EditText languageInp = (EditText) findViewById(R.id.langueInput);
                thisCountry.setmLanguage(languageInp.getText().toString());

                EditText popInp = (EditText) findViewById(R.id.popInput);
                thisCountry.setmPopulation(Integer.parseInt(popInp.getText().toString()));

                Toast.makeText(getApplicationContext(), "Modifications Saved", Toast.LENGTH_LONG).show();

                finish();
            }
        });


    }
}
